require(caret)
require(ggplot2)
require(randomForest)

confusionMatrix2BER<- function(confusion_matrix){
  TP <- confusion_matrix$table[1]
  FN <- confusion_matrix$table[2]
  FP <- confusion_matrix$table[3]
  TN <- confusion_matrix$table[4]
  BER <- 1/2*(FP/(TN+FP)+FN/(FN+TP))
  return(BER)
}
set.seed(17171)

setwd("D:\Users\jdestefani\workspace\ULB\TA\INFO-F-422\gitlab\Project\code")
MODELSDIR <- "models/"

# Data preprocessing
data <- read.csv("data/OnlineNewsPopularity.csv")
original_column_names <- colnames(data)
binary_variables_indicator <- grepl("weekday|data_channel",original_column_names)

numeric_columns <- data[,!binary_variables_indicator]
binary_columns <- data[,binary_variables_indicator]  

# Numeric columns

# Remove timedelta column, as non predictive
numeric_columns <- numeric_columns[,-c(2)]
# Replace URLs with numerical ids
numeric_columns$url <- 1:nrow(numeric_columns)

# Quantiles of the target variables
quantile(data$shares, probs = c(0.1,0.5,1,2,5,10,25,50,75,80,90,95,99)/100)

# Data preprocessing
scaled_data <- data.frame(scale(numeric_columns[,-c(1,ncol(numeric_columns))])) # Remove ID column and target column

# Classification
target <- ifelse(data$shares > 1400,1,0) # 50% -> 1400 80% -> 3400 90% -> 6200
scaled_data$target <- y 
scaled_data$shares <- NULL

# Regression
#scaled_data$target <- scaled_data$shares 
#scaled_data$shares <- NULL

results.df <- data.frame(method=character(),error_measure=character(),value=numeric()) 

# Data splitting (1/3 is roughly 10000 values)
# Caret ensures that the split preserves the variable distribution
InTrain<-createDataPartition(y=scaled_data$target,p=0.33,list=FALSE)

# Anonymize variables
colnames(scaled_data) <- outer("N",1:(ncol(scaled_data)),paste0)
colnames(binary_columns) <- outer("B",1:(ncol(binary_columns)),paste0)

# Save training, testing set, exact solution and random submission example
id <- numeric_columns$url
write.csv(data.frame(id=id[InTrain],scaled_data[InTrain,],target=target[InTrain,]), file = "train.csv",row.names = FALSE)
write.csv(data.frame(id=id[-InTrain],scaled_data[-InTrain,]), file = "test.csv",row.names = FALSE)
write.csv(data.frame(id=id[-InTrain],target=as.logical(rbinom(length(id[-InTrain]),size=1,prob=0.5))), file = "submission.csv",row.names = FALSE)
write.csv(data.frame(id=id[-InTrain],target=), file = "submission.csv",row.names = FALSE)

# Sanity check for distibution
par(mfrow=c(1,2))
plot(scaled_data[InTrain,]$target)
plot(scaled_data[-InTrain,]$target)

# Setting training control
trainControlCV <- trainControl(method="repeatedcv",
                               number=2, # Number of folds
                               repeats = 1, # Number of resampling per fold
                               allowParallel = TRUE,
                               verboseIter = TRUE)

#Setting model search grid using mtry
#mtry <- sqrt(ncol(scaled_data))
#tunegrid <- expand.grid(.mtry=mtry)

# Random forest training
# rf_model<-train(target~.,data=scaled_data[InTrain,],method="rf",
#                 trControl=trainControlCV, tuneGrid=tunegrid,
#                 prox=TRUE,allowParallel=TRUE)
rf_model<-train(target~.,data=scaled_data[InTrain,],method="rf",
                 trControl=trainControlCV, ntree=10,
                 prox=TRUE,allowParallel=TRUE)
print(rf_model)

predictions_rf <- predict(rf_model,scaled_data[-InTrain,])
postResample(pred = predictions_rf, obs = scaled_data[-InTrain,"target"])
results.df <- rbind(c("Random Forest","RMSE",TODO))

# # Classification
# confusion_matrix_rf <- confusionMatrix(predictions_rf,scaled_data[-InTrain,]$target)
# 
# # Balanced error rate - Slide 45 - Supervised classification
# BER <- 1/2 (FP/(TN+FP)+FN/(FN+TP))

# Variable importance (for random forest model)
variable_importances <- varImp(rf_model)$importance
row.names(variable_importances)[order(variable_importances, decreasing = TRUE)]


#Save model
saveRDS(rf_model, paste0(MODELSDIR,"randomForest80.rds"))

# Adaboost training
ada_model<-train(target~.,data=scaled_data[InTrain,],method="adaboost",
                trControl=trainControlCV,
                prox=TRUE,allowParallel=TRUE)
print(ada_model)

# Compute predictions
predictions_ada <- predict(ada_model,scaled_data[-InTrain,])
postResample(pred = predictions_ada, obs = scaled_data[-InTrain,"target"])
results.df <- rbind(c("Adaboost","RMSE",TODO))


#Save model
saveRDS(ada_model, paste0(MODELSDIR,"adaboost80.rds"))

# Nnet training
nnet_model<-train(target~.,data=scaled_data[InTrain,],method="nnet",
                 trControl=trainControlCV,
                 prox=TRUE,allowParallel=TRUE)
print(nnet_model)

predictions_nnet <- predict(nnet_model,scaled_data[-InTrain,])
postResample(pred = predictions_nnet, obs = scaled_data[-InTrain,"target"])
results.df <- rbind(c("nnet","RMSE",TODO))


#Save model
saveRDS(nnet_model, paste0(MODELSDIR,"nnet80.rds"))


# SVM training
svm_model<-train(target~.,data=scaled_data[InTrain,],method="svmRadial",
                 trControl=trainControlCV,
                 prox=TRUE,allowParallel=TRUE)
print(svm_model)

predictions_SVM <- predict(SVM_model,scaled_data[-InTrain,])
postResample(pred = predictions_nnet, obs = scaled_data[-InTrain,"target"])
results.df <- rbind(c("SVM","RMSE",TODO))


#Save model
saveRDS(svm_model, paste0(MODELSDIR,"svm80.rds"))
