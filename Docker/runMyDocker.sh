#!/bin/bash

nvidia-docker run --shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 -it --rm -v `pwd`/shared_data:/home/guest/shared_data:Z -p 9018:8888 jdestefani/infof422:test
